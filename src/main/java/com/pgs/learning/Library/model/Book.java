package com.pgs.learning.Library.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@Entity
@Table(name = "book")
@Getter
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    @Setter
    private int bookId;
    @Column(name = "title")
    @Setter
    private String title;
    @Column(name = "publication_date")
    @Setter
    private Date publicationDate;
    @Column(name = "copies_owned")
    @Setter
    private int copiesOwned;


    @ManyToOne
    @JoinColumn(name = "category_id")
    @Setter
    private Category category;


    @ManyToMany(mappedBy = "books")
    @Setter(AccessLevel.NONE)
    private List<Author> authors;

}
