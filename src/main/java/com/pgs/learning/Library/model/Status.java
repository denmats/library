package com.pgs.learning.Library.model;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@RequiredArgsConstructor
@Table(name = "status")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "status_id")
    @Setter
    @Getter
    private int statusId;
    @Column(name = "status_value")
    @Setter
    @Getter
    private String statusValue;

    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL)
    @Setter(AccessLevel.NONE)
    private List<Member> members;

}
