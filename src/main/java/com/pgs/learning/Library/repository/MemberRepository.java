package com.pgs.learning.Library.repository;

import com.pgs.learning.Library.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Integer> {
}
