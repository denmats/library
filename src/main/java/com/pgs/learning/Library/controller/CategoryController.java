package com.pgs.learning.Library.controller;

import com.pgs.learning.Library.dto.CategoryDTO;
import com.pgs.learning.Library.exception.CategoryNotFoundException;
import com.pgs.learning.Library.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService service;

    @GetMapping("/categories")
    @ExceptionHandler(CategoryNotFoundException.class)
    public List<CategoryDTO> getBookCategories() throws CategoryNotFoundException {
       return service.getBookCategories();
    }
}
