package com.pgs.learning.Library.controller;


import com.pgs.learning.Library.dto.MemberDTO;
import com.pgs.learning.Library.exception.MemberNotFoundExceptionLibraryApp;
import com.pgs.learning.Library.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class MemberController {

        private final MemberService memberService;

        @GetMapping("/members")
        @ExceptionHandler(MemberNotFoundExceptionLibraryApp.class)
        public List<MemberDTO> getAllMembers() throws MemberNotFoundExceptionLibraryApp {
                return memberService.findAll();
        }

        @GetMapping("/members/{id}")
        @ResponseBody
        @ExceptionHandler(MemberNotFoundExceptionLibraryApp.class)
        public MemberDTO getMemberById(@PathVariable("id") Integer member_id) throws MemberNotFoundExceptionLibraryApp {
                return memberService.findById(member_id);
        }

        @PostMapping("/members")
        @ResponseStatus(HttpStatus.CREATED)
        public ResponseEntity<MemberDTO> saveMember(@RequestBody MemberDTO memberDTO){
                memberService.save(memberDTO);
                return ResponseEntity.status(HttpStatus.CREATED)
                        .body(memberDTO);
        }

        @PutMapping("/members/{id}")
        @ResponseStatus(HttpStatus.OK)
        public ResponseEntity<MemberDTO> updateMember(@PathVariable("id") Integer memberId,
                               @RequestBody MemberDTO memberDTO){
                memberService.updateMemberDetails(memberId, memberDTO);
                return ResponseEntity.noContent().build();
        }

        @DeleteMapping("/members/{id}")
        @ResponseStatus(HttpStatus.OK)
        public ResponseEntity<MemberDTO> removeMember(@PathVariable("id") Integer memberId){
                memberService.removeMemberById(memberId);
                return ResponseEntity.noContent().build();
        }
}
