package com.pgs.learning.Library.controller;


import com.pgs.learning.Library.dto.BookCategoryDTO;
import com.pgs.learning.Library.dto.BookDTO;
import com.pgs.learning.Library.exception.BookNotFoundException;
import com.pgs.learning.Library.model.Book;
import com.pgs.learning.Library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping(path = "/books")
    @ExceptionHandler(BookNotFoundException.class)
    public List<BookDTO> getAllBooks() throws BookNotFoundException{
        return bookService.getAllBooks();
    }

    @GetMapping("/books/{id}")
    @ResponseBody
    @ExceptionHandler(BookNotFoundException.class)
    public BookDTO getBookById(@PathVariable("id") Integer bookId) throws BookNotFoundException{
        return bookService.findBookDtoById(bookId);
    }

    @PostMapping("/books")
    public ResponseEntity<BookDTO> saveBook(@RequestBody BookDTO bookDTO){
        bookService.saveBook(bookDTO);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(bookDTO);
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<BookDTO> removeBookById(@PathVariable("id") Integer bookId){
        bookService.removeBookById(bookId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<BookDTO> updateBookDetails(@PathVariable("id") Integer id,
            @RequestBody BookDTO bookDTO){
        bookService.updateBookDetails(id, bookDTO);
        return ResponseEntity.noContent().build();
    }
}
