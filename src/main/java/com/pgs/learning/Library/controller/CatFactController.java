package com.pgs.learning.Library.controller;

import com.pgs.learning.Library.dto.CatFactDto;
import com.pgs.learning.Library.service.CatFactService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CatFactController {

    private final CatFactService catFactService;

    @GetMapping("/catfacts")
    public CatFactDto getCatFactFromWeb(){
       return catFactService.getCatFact();

    }


}
