package com.pgs.learning.Library.controller;


import com.pgs.learning.Library.dto.AuthorDTO;
import com.pgs.learning.Library.exception.AuthorNotFoundException;
import com.pgs.learning.Library.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping("/authors")
    @ExceptionHandler(AuthorNotFoundException.class)
    public List<AuthorDTO> getAuthors() throws AuthorNotFoundException {
       return authorService.getAuthors();
    }

    @PostMapping("/authors")
    @ResponseBody
    public ResponseEntity<AuthorDTO> saveAuthor(@RequestBody AuthorDTO authorDTO) {
        authorService.saveAuthor(authorDTO);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(authorDTO);
    }

    @GetMapping("/authors/{id}")
    @ResponseBody
    @ExceptionHandler(AuthorNotFoundException.class)
    public AuthorDTO getAuthorById(@PathVariable("id") Integer authorId) throws AuthorNotFoundException {
        return authorService.getAuthorById(authorId);
    }

    @PutMapping("/authors/{id}")
    public ResponseEntity<AuthorDTO> updateAuthor(@PathVariable("id") Integer authorId, @RequestBody AuthorDTO authorDTO){
        authorService.updateAuthorDetails(authorId, authorDTO);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/authors/{id}")
    public ResponseEntity<AuthorDTO> removeAuthorById(@PathVariable("id") Integer authorId){
        authorService.removeAuthorById(authorId);
        return ResponseEntity.noContent().build();
    }
}
