package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.CategoryDTO;
import com.pgs.learning.Library.mapper.CategoryMapper;
import com.pgs.learning.Library.model.Category;
import com.pgs.learning.Library.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public List<CategoryDTO> getBookCategories() {
        List<Category> categories = categoryRepository.findAll();
        return categories.stream()
                .map(categoryMapper::convertToDto)
                .collect(Collectors.toList());
    }
}
