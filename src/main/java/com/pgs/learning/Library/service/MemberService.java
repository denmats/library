package com.pgs.learning.Library.service;


import com.pgs.learning.Library.dto.MemberDTO;
import com.pgs.learning.Library.mapper.MemberMapper;
import com.pgs.learning.Library.model.Member;
import com.pgs.learning.Library.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;
    private final MemberMapper memberMapper;

    public List<MemberDTO> findAll() {
        List<Member> members = memberRepository.findAll();
        return members.stream()
                .map(memberMapper::convertToDto)
                .collect(Collectors.toList());

    }

    public MemberDTO findById(Integer memberId) {
        Optional<Member> memberOptional = memberRepository.findById(memberId);
        if (memberOptional.isPresent()) {
            return memberMapper.convertToDto(memberOptional.get());
        } else {
            throw new RuntimeException("Member with Id=" + memberId + " is not found.");
        }
    }


    public MemberDTO save(MemberDTO memberDTO) {
        Member member = memberMapper.convertToEntity(memberDTO);
        if (member != null) {
            return memberMapper.convertToDto(memberRepository.save(member));
        }
        return null;
    }


    public MemberDTO updateMemberDetails(Integer memberId, MemberDTO memberDTO) {
        if (!Objects.equals(memberId, memberDTO.getMemberId())) {
            throw new IllegalArgumentException("The ID's don't match.");
        }
        Member member = memberMapper.convertToEntity(memberDTO);
        memberRepository.save(member);
        return memberMapper.convertToDto(member);
    }


    public Integer removeMemberById(Integer memberId) {
        Optional<Member> memberOptional = memberRepository.findById(memberId);
        if(memberOptional.isPresent()){
            memberRepository.delete(memberOptional.get());
            return memberId;
        }
       return null;
    }
}
