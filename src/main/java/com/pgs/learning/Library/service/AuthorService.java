package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.AuthorDTO;
import com.pgs.learning.Library.mapper.AuthorMapper;
import com.pgs.learning.Library.model.Author;
import com.pgs.learning.Library.repository.AuthorRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class AuthorService {

    private final AuthorMapper authorMapper;
    private final AuthorRepository authorRepository;

    public List<AuthorDTO> getAuthors() {
        return authorRepository.findAll().stream()
                .map(authorMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public AuthorDTO saveAuthor(AuthorDTO authorDTO) {
        Author author = authorMapper.convertToEntity(authorDTO);
        Author savedAuthor = authorRepository.save(author);
        return authorMapper.convertToDto(savedAuthor);
    }

    public AuthorDTO getAuthorById(Integer authorId) {
        if (authorRepository.findById(authorId).isPresent()) {
            return authorMapper.convertToDto(authorRepository.findById(authorId).get());
        } else {
            log.warn("The author with id = " + authorId + " is not found in the database.");
            return null;
        }
    }

    public AuthorDTO updateAuthorDetails(Integer authorId, AuthorDTO authorDTO) {
        if (!Objects.equals(authorId, authorDTO.getAuthorId())) {
            throw new IllegalArgumentException("The ID's don't match.");
        }
        Author author = authorMapper.convertToEntity(authorDTO);
        authorRepository.save(author);
        return authorDTO;
    }

    public Integer removeAuthorById(Integer authorId) {
        authorRepository.findById(authorId).ifPresentOrElse(authorRepository::delete
                , () -> {log.warn("Author ID= " + authorId + " is not found.");});
        return authorId;
    }
}
