package com.pgs.learning.Library.service;


import com.pgs.learning.Library.dto.CatFactDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class CatFactService {
    private final RestTemplate restTemplate = new RestTemplate();

    public CatFactDto getCatFact(){
        String CAT_FACT_URL = "https://catfact.ninja/fact";
        return restTemplate.getForObject(CAT_FACT_URL, CatFactDto.class);
    }
}
