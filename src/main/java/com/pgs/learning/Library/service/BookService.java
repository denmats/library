package com.pgs.learning.Library.service;


import com.pgs.learning.Library.dto.BookDTO;
import com.pgs.learning.Library.mapper.BookMapper;
import com.pgs.learning.Library.model.Book;
import com.pgs.learning.Library.repository.BookRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class BookService {

    private final BookMapper bookMapper;
    private final BookRepository bookRepository;

    public List<BookDTO> getAllBooks() {
        return bookRepository.findAll().stream()
                .map(bookMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public BookDTO saveBook(BookDTO bookDTO) {
        Book book = bookMapper.convertToEntity(bookDTO);
        Book savedBook = bookRepository.save(book);
        return bookMapper.convertToDto(savedBook);
    }

    public BookDTO findBookDtoById(Integer bookId) {
        if (bookRepository.findById(bookId).isPresent()) {
            return bookMapper.convertToDto(bookRepository.findById(bookId).get());
        } else {
            log.warn("The book with id = " + bookId + " is not found in the database.");
            return null;
        }
    }

    public BookDTO updateBookDetails(Integer bookId, BookDTO bookDTO) {
        if (!Objects.equals(bookId, bookDTO.getBookId())) {
            throw new IllegalArgumentException("The ID's don't match.");
        }
        Book book = bookMapper.convertToEntity(bookDTO);
        bookRepository.save(book);
        return bookDTO;
    }

    public Integer removeBookById(Integer bookId) {
        bookRepository.findById(bookId).ifPresentOrElse(bookRepository::delete
                , () -> {log.warn("Author ID= " + bookId + " is not found.");});
        return bookId;
    }
}