package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.WeatherDto;
import com.pgs.learning.Library.webclient.weather.WeatherClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class WeatherService {

   private final WeatherClient weatherClient;

    public WeatherDto getWeather(){
       return weatherClient.getWeatherForCity("wroclaw");
    }

}
