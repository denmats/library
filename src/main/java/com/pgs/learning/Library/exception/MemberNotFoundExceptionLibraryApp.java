package com.pgs.learning.Library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MemberNotFoundExceptionLibraryApp extends RuntimeException{
    public MemberNotFoundExceptionLibraryApp(String errorMessage) {
        super(errorMessage);
    }
}
