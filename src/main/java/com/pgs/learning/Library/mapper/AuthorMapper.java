package com.pgs.learning.Library.mapper;

import com.pgs.learning.Library.dto.AuthorDTO;
import com.pgs.learning.Library.model.Author;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorMapper {

    private final ModelMapper modelMapper;

    public Author convertToEntity(AuthorDTO authorDTO) {
        return modelMapper.map(authorDTO, Author.class);
    }

    public  AuthorDTO convertToDto(Author author) {
        return modelMapper.map(author, AuthorDTO.class);
    }
}
