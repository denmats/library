package com.pgs.learning.Library.mapper;


import com.pgs.learning.Library.dto.MemberDTO;
import com.pgs.learning.Library.model.Member;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberMapper {

    private final ModelMapper modelMapper;

    public MemberDTO convertToDto(Member member) {
        return modelMapper.map(member, MemberDTO.class);
    }

    public Member convertToEntity(MemberDTO memberDTO) {
        return modelMapper.map(memberDTO, Member.class);
    }
}
