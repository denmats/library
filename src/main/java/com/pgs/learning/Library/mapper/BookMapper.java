package com.pgs.learning.Library.mapper;

import com.pgs.learning.Library.dto.BookDTO;
import com.pgs.learning.Library.model.Book;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookMapper {

    private final ModelMapper modelMapper;

    public Book convertToEntity(BookDTO bookDTO) {
        return modelMapper.map(bookDTO, Book.class);
    }

    public  BookDTO convertToDto(Book book) {
        return modelMapper.map(book, BookDTO.class);
    }
}
