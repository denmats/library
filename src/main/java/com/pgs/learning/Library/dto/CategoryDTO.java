package com.pgs.learning.Library.dto;

import lombok.Data;

import java.util.List;

@Data
public class CategoryDTO {
    private int categoryId;
    private String categoryName;
    private List<BookDTO> books;

}
