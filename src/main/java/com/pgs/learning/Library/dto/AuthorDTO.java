package com.pgs.learning.Library.dto;

import com.pgs.learning.Library.model.Book;
import lombok.Data;

import java.util.List;

@Data
public class AuthorDTO {
    private int authorId;
    private String firstName;
    private String lastName;
    private List<Book> books;
}
