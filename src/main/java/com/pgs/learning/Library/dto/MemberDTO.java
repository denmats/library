package com.pgs.learning.Library.dto;

import com.pgs.learning.Library.model.Status;
import lombok.Data;

import java.sql.Date;


@Data
public class MemberDTO {
    private int memberId;
    private String firstName;
    private String lastName;
    private Date joinedDate;
    private Status status;
}
