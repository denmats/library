package com.pgs.learning.Library.dto;

import lombok.Getter;

@Getter
public class CatFactDto {
    private String fact;
    private boolean statusVerified;
    private int sentCount;
}
