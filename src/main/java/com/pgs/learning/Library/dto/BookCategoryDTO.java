package com.pgs.learning.Library.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class BookCategoryDTO {
    private int bookId;
    private String title;
    private Date publicationDate;
    private int copiesOwned;
    private String categoryName;
}
