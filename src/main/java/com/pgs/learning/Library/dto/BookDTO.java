package com.pgs.learning.Library.dto;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class BookDTO {
    private int bookId;
    private String title;
    private Date publicationDate;
    private int copiesOwned;
    private String categoryName;
    private List<String> authorsFirstAndLastName;
}
