package com.pgs.learning.Library.webclient.catfact.dto;

import lombok.Getter;

@Getter
public class DailyCatFactsStatusDto {
    private boolean verified;
    private int sentCount;
}
