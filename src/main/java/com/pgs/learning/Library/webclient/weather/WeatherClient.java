package com.pgs.learning.Library.webclient.weather;

import com.pgs.learning.Library.dto.WeatherDto;
import com.pgs.learning.Library.webclient.weather.dto.OpenWeatherWeatherDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class WeatherClient {

    private static final String WEATHER_URL = "https://api.openweathermap.org/data/2.5/";
    private static final String API_KEY= "849e938f744b32c9a6bffdf19b31b895";
    private RestTemplate restTemplate = new RestTemplate();

    public WeatherDto getWeatherForCity(String city) {
        OpenWeatherWeatherDto openWeatherWeatherDto = callGetMethod(OpenWeatherWeatherDto.class, city);
        return WeatherDto.builder()
                .temperature(openWeatherWeatherDto.getMain().getTemp())
                .pressure(openWeatherWeatherDto.getMain().getPressure())
                .humidity(openWeatherWeatherDto.getMain().getHumidity())
                .windSpeed(openWeatherWeatherDto.getWind().getSpeed())
                .build();
    }

    private <T> T callGetMethod(Class<T> responseType,String city) {
        return restTemplate.getForObject(WEATHER_URL + "weather?appid={apikey}&units=metric&q={city}",
                responseType, API_KEY, city);
    }
}
