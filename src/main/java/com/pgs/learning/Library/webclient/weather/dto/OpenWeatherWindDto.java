package com.pgs.learning.Library.webclient.weather.dto;

import lombok.Getter;

@Getter
public class OpenWeatherWindDto {
    private float speed;
}
