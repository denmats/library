package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.CategoryDTO;
import com.pgs.learning.Library.dto.MemberDTO;
import com.pgs.learning.Library.mapper.CategoryMapper;
import com.pgs.learning.Library.model.Category;
import com.pgs.learning.Library.model.Member;
import com.pgs.learning.Library.repository.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class CategoryServiceTest {

    @Mock
    private CategoryMapper mockCategoryMapper;
    @Mock
    private CategoryRepository mockCategoryRepository;
    private CategoryService categoryService;

    @BeforeEach
    void setUp() {
        this.categoryService = new CategoryService(mockCategoryRepository, mockCategoryMapper);
    }

    @Test
    @DisplayName("happy path to get Categories presented")
    void shouldGetBookCategories() {
        //given
        List<Category> categories = List.of(new Category(1,"title1"),
                new Category(2,"title2"));
        CategoryDTO categoryDTO = new CategoryDTO();
        when(mockCategoryMapper.convertToDto(any(Category.class))).thenReturn(categoryDTO);
        when(mockCategoryRepository.findAll()).thenReturn(categories);
        //when
        List<CategoryDTO> categoryDTOList = categoryService.getBookCategories();
        //then
        assertEquals(categories.size(), categoryDTOList.size());
    }
}