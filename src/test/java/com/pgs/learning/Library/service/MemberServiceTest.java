package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.MemberDTO;
import com.pgs.learning.Library.mapper.MemberMapper;
import com.pgs.learning.Library.model.Member;
import com.pgs.learning.Library.repository.MemberRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MemberServiceTest {

    @Mock
    private MemberRepository mockMemberRepository;
    @Mock
    private MemberMapper mockMemberMapper;

    private MemberService memberService;

    @BeforeEach
    void setUp() {
        this.memberService = new MemberService(mockMemberRepository,
                mockMemberMapper);
    }

    @Test
    @DisplayName("happy path to find all members")
    void shouldFindAllMembers() {
        //given
        List<Member> members = List.of(new Member(), new Member());
        when(mockMemberRepository.findAll()).thenReturn(members);
        //when
        List<MemberDTO> memberDTOs = memberService.findAll();
        //then
        verify(mockMemberRepository).findAll();
        assertEquals(members.size(), memberDTOs.size());
    }

    @Test
    @DisplayName("unhappy path to find all members")
    void shouldNotFindAllMembers() {
        //given
        List<Member> members = List.of(new Member(), new Member());
        when(mockMemberRepository.findAll()).thenReturn(List.of(new Member()));
        //when
        List<MemberDTO> memberDTOs = memberService.findAll();
        //then
        verify(mockMemberRepository).findAll();
        assertNotEquals(members.size(), memberDTOs.size());
    }


    @Test
    @DisplayName("happy path to find a member by id")
    void findById() {
        //given
        Member member = new Member();
        member.setMemberId(10);
        MemberDTO memberDTO = new MemberDTO();
        memberDTO.setMemberId(10);
        when(mockMemberRepository.findById(any(Integer.class))).thenReturn(Optional.of(member));
        when(mockMemberMapper.convertToDto(any(Member.class))).thenReturn(memberDTO);
        //when
        MemberDTO expectedMemberDTOId = memberService.findById(member.getMemberId());
        //then
        verify(mockMemberMapper).convertToDto(eq(member));
        verify(mockMemberRepository).findById(member.getMemberId());
        assertEquals(member.getMemberId(), expectedMemberDTOId.getMemberId());
    }


    @Test
    @DisplayName("happy path to save a member by providing a valid Member object")
    void shouldSaveMember() {
        //given
        Member member = new Member();
        MemberDTO memberDTO = new MemberDTO();
        when(mockMemberMapper.convertToEntity(any(MemberDTO.class))).thenReturn(member);
        when(mockMemberMapper.convertToDto(any(Member.class))).thenReturn(memberDTO);
        when(mockMemberRepository.save(any(Member.class))).thenReturn(member);
        //when
        MemberDTO savedMemberDTO = memberService.save(memberDTO);
        //then
        verify(mockMemberMapper).convertToEntity(eq(memberDTO));
        verify(mockMemberMapper).convertToDto(eq(member));
        verify(mockMemberRepository).save(eq(member));
        assertEquals(memberDTO, savedMemberDTO);
    }

    @Test
    @DisplayName("unhappy path to save a member by providing a invalid Member object")
    void shouldNotSaveMember() {
        //given
        Member member = new Member();
        MemberDTO memberDTO = new MemberDTO();
        memberDTO.setMemberId(10);
        when(mockMemberMapper.convertToEntity(any(MemberDTO.class))).thenReturn(member);
        when(mockMemberMapper.convertToDto(any(Member.class))).thenReturn(new MemberDTO());
        when(mockMemberRepository.save(any(Member.class))).thenReturn(member);
        //when
        MemberDTO savedMemberDTO = memberService.save(memberDTO);
        //then
        verify(mockMemberMapper).convertToEntity(eq(memberDTO));
        verify(mockMemberMapper).convertToDto(eq(member));
        verify(mockMemberRepository).save(eq(member));
        assertNotEquals(memberDTO, savedMemberDTO);
    }


    @Test
    @DisplayName("happy path to update member details by providing Id and MemberDTO object")
    void shouldUpdateMemberDetails() {
        //given
        Member member = new Member();
        member.setMemberId(1);
        MemberDTO memberDTO = new MemberDTO();
        memberDTO.setMemberId(1);
        when(mockMemberMapper.convertToEntity(any(MemberDTO.class))).thenReturn(member);
        when(mockMemberRepository.save(member)).thenReturn(member);
        when(mockMemberMapper.convertToDto(member)).thenReturn(memberDTO);
        //when
        MemberDTO savedMemberDto = memberService.updateMemberDetails(member.getMemberId(), memberDTO);
        //then
        verify(mockMemberMapper).convertToEntity(eq(memberDTO));
        verify(mockMemberMapper).convertToDto(eq(member));
        verify(mockMemberRepository).save(eq(member));
        assertEquals(memberDTO, savedMemberDto);
    }

    @Test
    @DisplayName("unhappy path to update member details by providing Id " +
            "doesn't match MemberDTO object id needs to be updated")
    void shouldNotUpdateMemberDetails_throwRuntimeException() {
        //given
        Member member = new Member();
        member.setMemberId(1);
        MemberDTO memberDTO = new MemberDTO();
        memberDTO.setMemberId(10);
        //when
        //then
        Throwable exception = assertThrows(IllegalArgumentException.class,
                ()->memberService.updateMemberDetails(member.getMemberId(),
                        memberDTO));

        assertEquals(IllegalArgumentException.class, exception.getClass());
        assertEquals("The ID's don't match.", exception.getMessage());
    }


    @Test
    @DisplayName("happy path to remove a member by id provided")
    void shouldRemoveMember() {
        //given
        Member member = new Member();
        when(mockMemberRepository.findById(member.getMemberId())).thenReturn(Optional.of(member));
        //when
        Integer removedId = memberService.removeMemberById(member.getMemberId());
        //then
        verify(mockMemberRepository).findById(eq(member.getMemberId()));
        assertEquals(member.getMemberId(), removedId);
    }
}