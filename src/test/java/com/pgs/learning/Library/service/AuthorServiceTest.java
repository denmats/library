package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.AuthorDTO;
import com.pgs.learning.Library.mapper.AuthorMapper;
import com.pgs.learning.Library.model.Author;
import com.pgs.learning.Library.repository.AuthorRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthorServiceTest {

    @Mock
    private AuthorRepository mockAuthorRepository;
    @Mock
    private AuthorMapper mockAuthorMapper;
    private AuthorService authorService;

    @BeforeEach
    void setUp() {
        this.authorService = new AuthorService(mockAuthorMapper, mockAuthorRepository);
    }

    @Test
    @DisplayName("unhappy path of saving author due to null passing as argument instead of Author object")
    public void shouldNotSaveAuthor() {
        //given
        Author author = new Author();
        AuthorDTO authorDTO = new AuthorDTO();
        lenient().when(mockAuthorMapper.convertToEntity(any(AuthorDTO.class))).thenReturn(author);
        lenient().when(mockAuthorMapper.convertToDto(any(Author.class))).thenReturn(authorDTO);
        //when
        AuthorDTO resultAuthorDTO = authorService.saveAuthor(authorDTO);
        //then
        verify(mockAuthorRepository).save(eq(author));
        verify(mockAuthorMapper).convertToEntity(eq(authorDTO));
        assertNotEquals(authorDTO, resultAuthorDTO);
    }


    @Test
    @DisplayName("happy path of getting all authors")
    void shouldReturnAllAuthors() {
        //given
        List<Author> authors = List.of(new Author(), new Author());
        when(mockAuthorRepository.findAll()).thenReturn(authors);
        //when
        List<AuthorDTO> resultAuthorDTOs = authorService.getAuthors();
        //then;
        assertEquals(2, resultAuthorDTOs.size());
    }

    @Test
    @DisplayName("unhappy path of getting all authors")
    void shouldNotReturnAllAuthorsDueToInputMismatched() {
        //given
        List<Author> authors = List.of(new Author(), new Author());
        when(mockAuthorRepository.findAll()).thenReturn(new ArrayList<Author>());
        //when
        List<AuthorDTO> resultAuthorDTOs = authorService.getAuthors();
        //then;
        assertNotEquals(2, resultAuthorDTOs.size());
    }

    @Test
    @DisplayName("happy path of getting a specific authorDTO by author ID")
    void shouldReturnAuthorDTOWithIdEqualsOne() {
        //given
        Author author = new Author();
        author.setAuthorId(1);
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorId(1);
        int authorId = author.getAuthorId();
        when(mockAuthorRepository.findById(authorId)).thenReturn(Optional.of(author));
        when(mockAuthorMapper.convertToDto(any(Author.class))).thenReturn(authorDTO);
        //when
        AuthorDTO resultAuthorDTO = authorService.getAuthorById(authorId);
        //then;
        assertEquals(authorId, resultAuthorDTO.getAuthorId());
    }

    @Test
    @DisplayName("unhappy path of getting a specific authorDTO by author ID")
    void shouldNotReturnAuthorDTOWithIdEqualsOne_becauseAuthorIdEqualsTwo() {
        //given
        Author author = new Author();
        author.setAuthorId(2);
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorId(1);
        int authorId = author.getAuthorId();
        when(mockAuthorRepository.findById(authorId)).thenReturn(Optional.of(author));
        when(mockAuthorMapper.convertToDto(any(Author.class))).thenReturn(authorDTO);
        //when
        AuthorDTO resultAuthorDTO = authorService.getAuthorById(authorId);
        //then;
        assertNotEquals(authorId, resultAuthorDTO.getAuthorId());
    }


    @Test
    @DisplayName("happy path to update Author details")
    void shouldUpdateAuthorDetails() {
        //given
        Author author = new Author();
        author.setAuthorId(1);
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorId(1);
        when(mockAuthorMapper.convertToEntity(any(AuthorDTO.class))).thenReturn(author);
        when(mockAuthorRepository.save(any(Author.class))).thenReturn(author);
        //when
        AuthorDTO updatedAuthorDTO = authorService.updateAuthorDetails(author.getAuthorId(), authorDTO);
        //then
        verify(mockAuthorRepository).save(eq(author));
        verify(mockAuthorMapper).convertToEntity(eq(authorDTO));
        assertEquals(authorDTO, updatedAuthorDTO);
    }

    @Test
    @DisplayName("unhappy path to update Author details: throwing IllegalArgumentException expected")
    void canNotUpdateAuthorDetails_shouldThrowException() {
        //given
        Author author = new Author();
        author.setAuthorId(100);
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorId(1);
        Throwable exception = assertThrows(IllegalArgumentException.class,
                ()->authorService.updateAuthorDetails(author.getAuthorId(), authorDTO));
        assertEquals("The ID's don't match.", exception.getMessage());
    }


    @Test
    @DisplayName("happy path of removing author by providing author id")
    void shouldRemoveAuthorById() {
        //given
        Author author = new Author();
        when(mockAuthorRepository.findById(any(Integer.class))).thenReturn(Optional.of(author));
        //when
        int resultId = authorService.removeAuthorById(1);
        //then
        verify(mockAuthorRepository).delete(eq(author));
        assertEquals(1, resultId);
    }
}