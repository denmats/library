package com.pgs.learning.Library.service;

import com.pgs.learning.Library.dto.BookDTO;
import com.pgs.learning.Library.mapper.BookMapper;
import com.pgs.learning.Library.model.Book;
import com.pgs.learning.Library.model.Category;
import com.pgs.learning.Library.repository.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {

    @Mock
    private BookRepository mockBookRepository;
    @Mock
    private BookMapper mockBookMapper;

    private BookService bookService;

    @BeforeEach
    void setUp() {
        this.bookService = new BookService(mockBookMapper, mockBookRepository);
    }


    @Test
    void shouldReturnAllBooks() {
        //given
        Book book = new Book();
        BookDTO bookDTO = new BookDTO();
        when(mockBookMapper.convertToDto(any(Book.class))).thenReturn(bookDTO);
        List<Book> books = List.of(book);
        when(mockBookRepository.findAll()).thenReturn(books);
        //when
        List<BookDTO> booksFound = bookService.getAllBooks();
        //then
        assertEquals(books.size(), booksFound.size());
    }

    @Test
    void shouldFindBookById() {
        //given
        Book book = new Book();
        book.setCategory(new Category(1,"Category1"));
        BookDTO bookDTO = new BookDTO();
        when(mockBookMapper.convertToDto(book)).thenReturn(bookDTO);
        when(mockBookRepository.findById(book.getBookId())).thenReturn(Optional.of(book));
        //when
        BookDTO bookDtoFound = bookService.findBookDtoById(book.getBookId());
        //then
        assertEquals(book.getBookId(), bookDtoFound.getBookId());
    }

    @Test
    void shouldSaveBook(){
        //given
        Book book = new Book();
        BookDTO bookDTO = new BookDTO();
        when(mockBookMapper.convertToEntity(bookDTO)).thenReturn(book);
        when(mockBookRepository.save(any(Book.class))).thenReturn(book);
        when(mockBookMapper.convertToDto(book)).thenReturn(bookDTO);
        //when
        BookDTO savedBookDTO = bookService.saveBook(bookDTO);
        //then
        verify(mockBookRepository).save(eq(book));
        assertEquals(book.getBookId(), savedBookDTO.getBookId());
    }

    @Test
    void shouldRemoveBookById(){
        //given
        Book book = new Book();
        when(mockBookRepository.findById(any(Integer.class))).thenReturn(Optional.of(book));
        //when
        Integer removedId = bookService.removeBookById(book.getBookId());
        //then
        verify(mockBookRepository).findById(eq(book.getBookId()));
        assertEquals(book.getBookId(), removedId);
    }

    @Test
    void shouldUpdateBook(){
        //given
        BookDTO bookDto = new BookDTO();
        Book book = new Book();
        when(mockBookMapper.convertToEntity(bookDto)).thenReturn(book);
        when(mockBookRepository.save(any(Book.class))).thenReturn(book);
        //when
        BookDTO savedBook = bookService.updateBookDetails(book.getBookId(), bookDto);
        //then
        verify(mockBookRepository).save(eq(book));
        assertEquals(bookDto, savedBook);
    }
}