insert into category values(1, "Fantasy");
insert into category values(2,"Sci-Fi");
insert into category values(3, "Mystery");
insert into category values(4,"Thriller");
##
insert into book values(1, 2, "2000-01-01", "title1", 4);
insert into book values(2, 9, "2020-01-01", "title2", 2);
insert into book values(3, 4, "2010-03-11", "title3", 3);
insert into book values(4, 10, "2004-05-21", "title4", 4);
insert into book values(5, 2, "2014-11-01", "title5", 2);
insert into book values(6, 3, "2000-01-01", "title6", 4);
insert into book values(7, 2, "2000-01-01", "title7", 1);
##
insert into author values(1, "John", "Doe");
insert into author values(2, "Jack", "Sparrow");
##
insert into status values(1, "active");
insert into status values(2, "inactive");
insert into status values(3, "pending");
##
insert into member values(1, "Jane", "2022-11-23", "Smith",1);
insert into member values(2, "student2", "2023-03-22", "surname2",1);
insert into member values(3, "student3", "2020-11-11", "surname3",2);
insert into member values(4, "student4", "2023-06-02", "surname4",3);
