#!/usr/bin/env bash

set -euo pipefail
which docker > /dev/null || (echoerr "Please ensure that docker is in your PATH" && exit 1)

mkdir -p $HOME/docker/volumes/mysql
rm -rf $HOME/docker/volumes/mysql/data

sudo docker stop library
sudo docker rm library
