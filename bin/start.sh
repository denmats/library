#!/usr/bin/env bash

set -euo pipefail
which docker > /dev/null || (echoerr "Please ensure that docker is in your PATH" && exit 1)

mkdir -p $HOME/docker/volumes/mysql
rm -rf $HOME/docker/volumes/mysql/data

sudo docker run --name library -e MYSQL_ROOT_PASSWORD=denys -d -p 3306:3306 mysql
sleep 3

sudo docker exec -it library /bin/bash




